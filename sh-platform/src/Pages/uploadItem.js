import React, { Component } from 'react';
import { Form, Button, Table} from "react-bootstrap";
import { Container} from 'reactstrap';
import web3 from '../web3';
import contract from '../contract';
// import ipfs from '../ipfs';

const IPFS = require('ipfs')

class newItem extends React.Component {
    state = {
        name: "",
        description: "",
        price: 1,
        seller_addr: "",
        metaAccounts: null,
        ethAddress:'',
        buffer:'',
        imghash: ''
    }

    handleGetName = (event) => {
        this.setState({
          name : event.target.value,
        })
    };

    handleGetPrice = (event) => {
        var value = event.target.value;
        if(value<=0){
            value = 1;
        }
        var value_temp = event.target.value.toString();
        if(value_temp.includes(".")){
            value = Math.round(value);
        }
        this.setState({
          price : value,
        })
    };

    handleGetDescription = (event) => {
        this.setState({
            description : event.target.value,
        })
    };

    register = async (event) => {
        const ethAddress= await contract.options.address;
        this.setState({ethAddress});
        try{
          const accounts = await web3.eth.getAccounts();
          this.setState({metaAccounts:accounts})
          if (accounts.length==0){
            window.alert("Please login your metmask account. If you have logged in, please allow our site to access your accounts. (You can set this in Metamask -> Setting -> Connection)");
          }
          console.log('Sending from Metamask account: ' + accounts[0]);
        }catch(error){
          console.log(error);
          window.alert("You haven't configured the crypto wallet app of Ethereum, Please get the Metamask add-on first and sign up/log in the MetaMask account.");
        }
    
        contract.methods.register().send({
          from: this.state.metaAccounts[0]
        },(error, transactionHash)=>{
          console.log(transactionHash);
        });
    }

    captureFile = async (event) => {
        event.stopPropagation()
        event.preventDefault()
        const file = event.target.files[0]
        let reader = new window.FileReader()
        reader.readAsArrayBuffer(file)
        reader.onloadend = () => this.convertToBuffer(reader)    
    };
    convertToBuffer = async(reader) => {
        //file is converted to a buffer for upload to IPFS
          const buffer = await Buffer.from(reader.result);
        //set this buffer -using es6 syntax
          this.setState({buffer});
        //   console.log(this.state.buffer.toString());
        //   console.log(this.state.buffer);
    };
    
    upload = async(event) => {
        try{
            const accounts = await web3.eth.getAccounts();
            this.setState({metaAccounts:accounts})
            if (accounts.length==0){
              window.alert("Please login your metmask account. If you have logged in, please allow our site to access your accounts. (You can set this in Metamask -> Setting -> Connection)");
            }
            console.log('Sending from Metamask account: ' + accounts[0]);
          }catch(error){
            console.log(error);
            window.alert("You haven't configured the crypto wallet app of Ethereum, Please get the Metamask add-on first and sign up/log in the MetaMask account.");
          }

        const node = await IPFS.create()
        const vers = await node.version()
        console.log('Version:', vers.version)

        for await (const file of await node.add({
            content: this.state.buffer
          })) {
            console.log('Added file:', file.path)
            this.setState({imghash: file.path})
          }

        console.log("img hash: "+ this.state.imghash)

        // post_item(string memory picture, string memory name, string memory description, uint price)
        contract.methods.post_item(this.state.imghash, this.state.name, this.state.description, this.state.price).send({
            from: this.state.metaAccounts[0]
        },(error, transactionHash)=>{
            console.log(transactionHash);
        }); // contract
          
    }

    render() {
        return (
            <div className="App">
                <Container>
                    <div className='header'>
                        <div id="right" style={{width: '67%', float:'left'}}>
                            <h3 className="txt"><b> SH Selling Platform </b></h3>  
                        </div>
                        
                        <div style={{width: '13%', float:'left', marginTop: '2%', height:'5%',}}>
                            <a href='/'><Button className= "btn" >RETURN HOME</Button></a>
                        </div>

                        <div style={{width: '20%', float:'left', marginTop: '2%', height:'5%',}}>
                            <Button  onClick = {this.register}> REGISTER</Button>
                        </div>
                    </div>    
                    <h2 style={{fontFamily: 'Monospace'}}> Please Enter Item Info Here</h2>
                    <label for = "name" style={{fontFamily: 'Monospace', fontSize: "16px"}}>Item Name </label>
                    <input type='text' id= 'name'
                        placeholder = {'Please enter item name here'}
                        value={this.state.name}
                        onChange={this.handleGetName}
                        style={{ 
                            marginLeft: "5%",
                            width: '300px',
                            borderRadius: 5, 
                            borderWidth: 1, 
                            padding: 5,
                            borderStyle: 'solid',
                            fontFamily: 'Monospace',
                            borderColor: '#797D7F'}}
                        ></input>
                    <br></br>
                    <br></br>
                    <label for = "description" style={{fontFamily: 'Monospace', fontSize: "16px"}}>Description </label>
                    <textarea  rows="5" cols="20" id= 'description'
                        placeholder = {'Please enter item description here'}
                        value={this.state.description}
                        onChange={this.handleGetDescription}
                        style={{ 
                            width: '300px',
                            marginLeft: "4%",
                            borderRadius: 5, 
                            borderWidth: 1, 
                            padding: 5,
                            borderStyle: 'solid',
                            fontFamily: 'Monospace',
                            borderColor: '#797D7F'}}
                        ></textarea>
                    <br></br>
                    <br></br>
                    <label for = "price" style={{fontFamily: 'Monospace', fontSize: "16px"}}>Item Price(finney) </label>
                    <input type='number' id= 'price' min= '1'
                        placeholder = {'Please choose item price here (Only Positive Integer)'}
                        value={this.state.price}
                        onChange={this.handleGetPrice}
                        style={{ 
                            marginLeft: "1%",
                            width: '300px',
                            borderRadius: 5, 
                            borderWidth: 1, 
                            padding: 5,
                            borderStyle: 'solid',
                            fontFamily: 'Monospace',
                            borderColor: '#797D7F'}}
                        ></input>
                    {/* <br></br> */}
                    <p style={{marginLeft: "6%", fontFamily: 'Monospace'}}>Only positive integer for price</p>
                    <label for = "img" style= {{fontFamily: 'Monospace', fontSize: "16px", marginRight: "1%"}}> Item Picture </label>
                    <input 
                        type = "file"
                        id = "img"
                        onChange = {this.captureFile}
                        style={{
                            marginLeft: "1%"
                        }}
                    />
                    <Button 
                    onClick = {this.upload}
                    > 
                    Sell
                    </Button>
                    <p style= {{fontFamily: 'Monospace', fontSize: "16px", marginTop: "3%"}}>If you found your transaction fails, that maybe because you haven't registered yet.</p>
                    <p style= {{fontFamily: 'Monospace', fontSize: "16px"}}>Please click the register button on the top right corner and refresh the page after the transaction is showed as confirmed in metamask.</p>
                </Container>
            </div>
        )
    }
}

export default newItem;