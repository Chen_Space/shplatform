import React, { Component } from 'react';
import { Form, Button, Table} from "react-bootstrap";
import { Container} from 'reactstrap';
import { Route,Link, NavLink } from 'react-router-dom'
import '../App.css';
import web3 from '../web3';
import contract from '../contract';


class ratingHistory extends React.Component{
    state={
        seller: '',
        history: []
    }

    constructor(props){
        super(props);
    }

    async componentDidMount() {
        this.setState({seller: this.props.match.params.seller})
        console.log(this.props.match.params.seller);
        try{
            const accounts = await web3.eth.getAccounts();
            this.setState({metaAccounts:accounts})
            if (accounts.length==0){
              window.alert("Please login your metmask account. If you have logged in, please allow our site to access your accounts. (You can set this in Metamask -> Setting -> Connection)");
            }
            console.log('Sending from Metamask account: ' + accounts[0]);
        }catch(error){
        console.log(error);
        window.alert("You haven't configured the crypto wallet app of Ethereum, Please get the Metamask add-on first and sign up/log in the MetaMask account.");
        }
        var object = this;

        // function rate (string memory rating, address seller_addr) public purchsedItem(msg.sender, seller_addr)
        contract.methods.getRatingHistory(this.state.seller).call({
            from: this.state.metaAccounts[0]
        }).then(function(result){
            console.log(result);
            object.setState({history: result});
        }); // contract
        console.log("Returned data is: "+ this.state.history);
    }




    register = async (event) => {
        const ethAddress= await contract.options.address;
        this.setState({ethAddress});
        try{
          const accounts = await web3.eth.getAccounts();
          this.setState({metaAccounts:accounts})
          if (accounts.length==0){
            window.alert("Please login your metmask account. If you have logged in, please allow our site to access your accounts. (You can set this in Metamask -> Setting -> Connection)");
          }
          console.log('Sending from Metamask account: ' + accounts[0]);
        }catch(error){
          console.log(error);
          window.alert("You haven't configured the crypto wallet app of Ethereum, Please get the Metamask add-on first and sign up/log in the MetaMask account.");
        }
    
        contract.methods.register().send({
          from: this.state.metaAccounts[0]
        },(error, transactionHash)=>{
          console.log(transactionHash);
        });
    }

    

    render() {
      if(this.state.history.length == 0){

        return (
          <div className="App">
            <Container>
              <div className = 'header'>
                <div id="right" style={{width: '67%', float:'left'}}>
                    <h3 className="txt"> SH Selling Platform</h3> 
                </div>
                
                <div style={{width: '13%', float:'left', marginTop: '1%'}}>
                            <a href='/'><Button className= "btn" >RETURN HOME</Button></a>
                </div>
  
                <div style={{width: '20%', float:'left', marginTop: '1%'}}>
                  <Button  onClick = {this.register}> REGISTER</Button>
                </div>
              </div> 
  
              <h2 style={{fontFamily: 'Monospace'}}> Following Items are the rating History of {this.state.seller}</h2>
              <p style={{fontFamily: 'Monospace', fontSize: '20px'}}> No rating history yet</p>   
            </Container>
          </div>
        )
      }else{

        return (
          <div className="App">         
          <Container>
              <div className='header'>
                  <div id="right" style={{width: '67%', float:'left'}}>
                      <h3 className="txt"><b> SH Selling Platform </b></h3>  
                  </div>
                  
                  <div style={{width: '13%', float:'left', marginTop: '2%', height:'5%',}}>
                      <a href='/'><Button className= "btn" >RETURN HOME</Button></a>
                  </div>
  
                  <div style={{width: '20%', float:'left', marginTop: '2%', height:'5%',}}>
                      <Button  onClick = {this.register}> REGISTER</Button>
                  </div>
              </div>  
        <h2 style={{fontFamily: 'Monospace'}}> Following Items are the rating History of {this.state.seller}</h2>
              {
                  this.state.history.map(item =>(
                      <li key={item} style={{fontFamily: 'Monospace', fontSize: "20px"}}>{item} stars</li>  
                  ))
              }
          </Container>
          </div>
        )
      }
      
    }
  
    
}

export default ratingHistory;