import React, { Component } from 'react';
import { Form, Button, Table} from "react-bootstrap";
import { Container} from 'reactstrap';
import { Route,Link, NavLink } from 'react-router-dom'
import '../App.css';
import web3 from '../web3';
import contract from '../contract';

class App extends Component{
  state = {
    name: "",
    description: "",
    price: 0,
    seller_addr: "",
    metaAccounts: null,
    ethAddress:'',
    seller: '',
    items: []
  }

  constructor(props){
        super(props);
    }

  async componentDidMount() {
    var object = this;

    contract.methods.getAllItems().call().then(function(result){
          console.log(result);
          object.setState({items: result});
      }); // contract
      console.log("items: "+ this.state.items);
  }

  register = async (event) => {
    const ethAddress= await contract.options.address;
    this.setState({ethAddress});
    try{
      const accounts = await web3.eth.getAccounts();
      this.setState({metaAccounts:accounts})
      if (accounts.length==0){
        window.alert("Please login your metmask account. If you have logged in, please allow our site to access your accounts. (You can set this in Metamask -> Setting -> Connection)");
      }
      console.log('Sending from Metamask account: ' + accounts[0]);
    }catch(error){
      console.log(error);
      window.alert("You haven't configured the crypto wallet app of Ethereum, Please get the Metamask add-on first and sign up/log in the MetaMask account.");
    }

    contract.methods.register().send({
      from: this.state.metaAccounts[0]
    },(error, transactionHash)=>{
      console.log(transactionHash);
    });
  }

  

  jumpToDetails = async (event, imgHash) =>{
    var path = "/showItem/"+ imgHash;
    this.props.history.push(path);
  }

  jumpToRate = async (event,seller_addr) =>{
    // var seller_addr = '0xF8c9cd2b6c4261618ef1fC2Ef46E42CaF1640Aa2'; //get all item 功能实现后 此处改为 this.state.seller
    var path = "/rating/"+ seller_addr;
    this.props.history.push(path);
  }

  render() {
    if(this.state.items.length == 0){
      return (
        <div className="App">
          <Container>
            <div className = 'header'>
              <div id="right" style={{width: '67%', float:'left'}}>
                  <h3 className="txt"> SH Selling Platform</h3> 
              </div>
              
              <div style={{width: '13%', float:'left', marginTop: '1%'}}>
                <a href='/#/uploadItem'><Button className= "btn" >SELL ITEM</Button></a>
              </div>

              <div style={{width: '20%', float:'left', marginTop: '1%'}}>
                <Button  onClick = {this.register}> REGISTER</Button>
              </div>
            </div> 

            <p style={{fontFamily: 'Monospace', fontSize: '26px'}}> No item to be sold yet</p>   
          </Container>
        </div>
      )
    }else{
      return (
        <div className="App">
        {/* <header className="App-header">
          <h1> SHPlatform</h1>
          <h3>A Blockchain Based Second-Hand Selling Platform</h3>
        </header> */}
        
          <Container>
            <div className = 'header'>
              <div id="right" style={{width: '67%', float:'left'}}>
                  <h3 className="txt"> SH Selling Platform</h3> 
              </div>
              
              <div style={{width: '13%', float:'left', marginTop: '1%'}}>
                <a href='/#/uploadItem'><Button className= "btn" >SELL ITEM</Button></a>
              </div>

              <div style={{width: '20%', float:'left', marginTop: '1%'}}>
                <Button  onClick = {this.register}> REGISTER</Button>
              </div>
            </div>    
        

            <h2 style={{fontFamily: 'Monospace'}}> Items Info (Please click for details)</h2>
                {
                  this.state.items.map(item =>(
                      <li key={item} style={{margin:'1%', fontFamily: 'Monospace', fontSize: '18px'}}>{item[1] + "  "+item[3] + "finney"} <button style={{marginLeft: '2%'}} onClick={(ev) => {this.jumpToDetails(ev, item[0])}}> Details </button></li>              
                  ))
                }

        </Container>
      </div>
    )
    }
    
  }

  
}

export default App;
