import React, { Component } from 'react';
import { Form, Button, Table} from "react-bootstrap";
import { Container} from 'reactstrap';
import { Route,Link, NavLink } from 'react-router-dom'
import '../App.css';
import web3 from '../web3';
import contract from '../contract';


class rating extends React.Component{
    state={
        seller: '',
        rating: 1
    }

    constructor(props){
        super(props);
    }

    componentDidMount() {
        //console.log(this.props.match.params);
        this.setState({seller: this.props.match.params.seller})
        console.log(this.props.match.params.seller);
    }

    handleGetRating = (event) => {
        var value = event.target.value;
        if(value<1){
            value = 1;
        }
        if(value<1){
            value = 10;
        }
        this.setState({
            rating : value,
        })
    };


    register = async (event) => {
        const ethAddress= await contract.options.address;
        this.setState({ethAddress});
        try{
          const accounts = await web3.eth.getAccounts();
          this.setState({metaAccounts:accounts})
          if (accounts.length==0){
            window.alert("Please login your metmask account. If you have logged in, please allow our site to access your accounts. (You can set this in Metamask -> Setting -> Connection)");
          }
          console.log('Sending from Metamask account: ' + accounts[0]);
        }catch(error){
          console.log(error);
          window.alert("You haven't configured the crypto wallet app of Ethereum, Please get the Metamask add-on first and sign up/log in the MetaMask account.");
        }
    
        contract.methods.register().send({
          from: this.state.metaAccounts[0]
        },(error, transactionHash)=>{
          console.log(transactionHash);
        });
    }

    
    rate = async (event) => {
        try{
            const accounts = await web3.eth.getAccounts();
            this.setState({metaAccounts:accounts})
            if (accounts.length==0){
              window.alert("Please login your metmask account. If you have logged in, please allow our site to access your accounts. (You can set this in Metamask -> Setting -> Connection)");
            }
            console.log('Sending from Metamask account: ' + accounts[0]);
        }catch(error){
        console.log(error);
        window.alert("You haven't configured the crypto wallet app of Ethereum, Please get the Metamask add-on first and sign up/log in the MetaMask account.");
        }
        var rating_str = this.state.rating.toString();
        // function rate (string memory rating, address seller_addr) public purchsedItem(msg.sender, seller_addr)
        contract.methods.rate(rating_str, this.state.seller).send({
            from: this.state.metaAccounts[0]
        },(error, transactionHash)=>{
            console.log(transactionHash);
        }); // contract
        
    }

    render() {
      return (
        <div className="App">
          
        <Container>
            <div className='header'>
                <div id="right" style={{width: '67%', float:'left'}}>
                    <h3 className="txt"><b> SH Selling Platform </b></h3>  
                </div>
                
                <div style={{width: '13%', float:'left', marginTop: '2%', height:'5%',}}>
                    <a href='/'><Button className= "btn" >RETURN HOME</Button></a>
                </div>

                <div style={{width: '20%', float:'left', marginTop: '2%', height:'5%',}}>
                    <Button  onClick = {this.register}> REGISTER</Button>
                </div>
            </div>  
            <h2 style={{fontFamily: 'Monospace'}}> Congratulations! You have purchased this item successfully!</h2>
            <h2 style={{fontFamily: 'Monospace'}}> Reminder: Please check your transaction in metamask. When it is confirmed, you can start rate.</h2>
            <h2 style={{fontFamily: 'Monospace'}}> Please rate this seller here</h2>
            <label style={{fontFamily: 'Monospace', fontSize: "16px"}}> Rating</label> 
            <input type='number' min= '1' max = '10'
                    value={this.state.rating}
                    onChange={this.handleGetRating}
                    style={{ 
                        marginLeft: "1%",
                        width: '150px',
                        borderRadius: 5, 
                        borderWidth: 1, 
                        padding: 5,
                        borderStyle: 'solid',
                        fontFamily: 'Monospace',
                        borderColor: '#797D7F'}}
                    ></input> 
                <p style={{marginLeft: "6%", fontFamily: 'Monospace', marginRight: "2%"}}>Range: 1 - 10</p>
                <br></br>
            <Button onClick = {this.rate}>Submit Rate</Button>
        </Container>
        </div>
      )
    }
  
    
}

export default rating;