import React, { Component } from 'react';
import { Form, Button, Table} from "react-bootstrap";
import { Container} from 'reactstrap';
import { Route,Link, NavLink } from 'react-router-dom'
import '../App.css';
import web3 from '../web3';
import contract from '../contract';

const IPFS = require('ipfs')

class showItem extends React.Component{
    state={
        imgHash: '',
        itemInfo: null,
        img: '',
        seller_addr: '',
        name: '',
        description: '',
        price: 0
    }

    constructor(props){
        super(props);
    }

    async componentWillMount() {
        //console.log(this.props.match.params);
        this.setState({imgHash: this.props.match.params.imgHash})
        console.log(this.props.match.params.imgHash);

        try{
            const accounts = await web3.eth.getAccounts();
            this.setState({metaAccounts:accounts})
            if (accounts.length==0){
              window.alert("Please login your metmask account. If you have logged in, please allow our site to access your accounts. (You can set this in Metamask -> Setting -> Connection)");
            }
            console.log('Sending from Metamask account: ' + accounts[0]);
        }catch(error){
        console.log(error);
        window.alert("You haven't configured the crypto wallet app of Ethereum, Please get the Metamask add-on first and sign up/log in the MetaMask account.");
        }
        var object = this;

        //getItem(string memory picture_hash) public view returns (Item memory)
        contract.methods.getItem(this.state.imgHash).call({
            from: this.state.metaAccounts[0]
        }).then(function(result){
            console.log(result);
            object.setState({itemInfo: result}); // sequence: picture, name, description, price, seller
            object.setState({name: object.state.itemInfo[1]});
            object.setState({description: object.state.itemInfo[2]});
            object.setState({price: parseInt(object.state.itemInfo[3])});
            object.setState({seller_addr: object.state.itemInfo[4]});
        }); // contract

        const node = await IPFS.create();
        const version = await node.version();
        console.log('Version:', version.version);

        const chunks = []
        console.log("ImgHash in seeImg is: " + this.state.imgHash);
        for await (const chunk of node.cat(this.state.imgHash)) {
            chunks.push(chunk)
        }
        var imgBase64 = "data:image/png;base64," + Buffer.concat(chunks).toString('base64');
        this.setState({img: imgBase64})
    }

    Buy = async (event,seller_addr) =>{
        try{
            const accounts = await web3.eth.getAccounts();
            this.setState({metaAccounts:accounts})
            if (accounts.length==0){
              window.alert("Please login your metmask account. If you have logged in, please allow our site to access your accounts. (You can set this in Metamask -> Setting -> Connection)");
            }
            console.log('Sending from Metamask account: ' + accounts[0]);
        }catch(error){
            console.log(error);
            window.alert("You haven't configured the crypto wallet app of Ethereum, Please get the Metamask add-on first and sign up/log in the MetaMask account.");
        }

        var price = this.state.price.toString();
        console.log('price: '+ web3.utils.toWei(price, "finney"));
        //function purchase (uint price, address seller_addr) public payable
        contract.methods.purchase(price, this.state.seller_addr, this.state.imgHash).send({
            from: this.state.metaAccounts[0],
            value: web3.utils.toWei(price, "finney")
        },(error, transactionHash)=>{
            console.log(transactionHash);
        }); // contract

        // var seller_addr = '0xF8c9cd2b6c4261618ef1fC2Ef46E42CaF1640Aa2'; //get all item 功能实现后 此处改为 this.state.seller
        var path = "/rating/"+ seller_addr;
        this.props.history.push(path);
      }


    
    jumpToRatingHistory = async (event,seller_addr) =>{
        // var seller_addr = '0xF8c9cd2b6c4261618ef1fC2Ef46E42CaF1640Aa2';  // get all item 功能实现后 此处改为 this.state.seller
        var path = "/ratingHistory/"+ seller_addr;
        this.props.history.push(path);
      }

    render() {
        return(
            <div className="App">
                <Container>
                    <div className='header'>
                        <div id="right" style={{width: '67%', float:'left'}}>
                            <h3 className="txt"><b> SH Selling Platform </b></h3>  
                        </div>
                        
                        <div style={{width: '13%', float:'left', marginTop: '2%', height:'5%',}}>
                            <a href='/'><Button className= "btn" >RETURN HOME</Button></a>
                        </div>

                        <div style={{width: '20%', float:'left', marginTop: '2%', height:'5%',}}>
                            <Button  onClick = {this.register}> REGISTER</Button>
                        </div>
                        
                    </div>   
                    <h1 style={{fontFamily: 'Monospace'}}>Item Specifications</h1>
                    <img src={this.state.img}></img>
                    <p style={{fontFamily: 'Monospace', fontSize: '16px'}}>Item Name: {this.state.name}</p>
                    <p style={{fontFamily: 'Monospace', fontSize: '16px', width: '30%',marginLeft: '35%'}}>Item Description: <br></br> {this.state.description}</p>
                    <p style={{fontFamily: 'Monospace', fontSize: '16px'}}>Item Price: {this.state.price} finney</p>
                    <p style={{fontFamily: 'Monospace', fontSize: '16px'}}>Item Seller Address: {this.state.seller_addr}</p>
                    <Button className= "btn" onClick={(ev) => {this.Buy(ev, this.state.seller_addr)}}>BUY</Button> <br></br>
                    <Button className= "btn" style={{marginTop: '1%', marginBottom: '2%'}} onClick={(ev) => {this.jumpToRatingHistory(ev, this.state.seller_addr)}}>RATING HISTORY</Button>
                    
                </Container>
                
            </div>
        )
    }

}

export default showItem;