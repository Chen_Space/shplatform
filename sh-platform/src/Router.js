import React from 'react';
import {HashRouter, Route, Switch} from 'react-router-dom';

import App from './Pages/App';
import newItem from './Pages/uploadItem';
import rating from './Pages/rating';
import ratingHistory from './Pages/ratingHistory';
import showItem from './Pages/showItem';


const BasicRoute = () => (
    <HashRouter>
        <Switch>
            <Route exact path="/" component={App}/>
            <Route exact path="/rating/:seller" component={rating}/>
            <Route exact path="/ratingHistory/:seller" component={ratingHistory}/>
            <Route exact path="/uploadItem" component={newItem}/>
            <Route exact path="/showItem/:imgHash" component={showItem}/>
        </Switch>
    </HashRouter>
);


export default BasicRoute;