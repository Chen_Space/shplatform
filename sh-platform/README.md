# Second-hand Selling Platform

IERG5590-IEMS5809 Regular Project Report

Mengjie Chen (1155130351), Xiao Yi (1155138489)

## 1. How to Run the Project

### 1.1 Install Required Packages

First of all, you should check if ```npm``` is installed by input ```npm -v```. If not, please follow this [link](https://nodejs.org/en/) to install ```npm```. 

Then go to the project directory by ```cd sh-platform``` then run ```npm install``` to install all site-packages required.

### 1.2 Config MetaMask Account

The project requires an MetaMask account to register, sell items, purchase items and rate sellers. You can install the [MetaMask](https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn) Chrome extension in Chrome's extension store. Once done, register a MetaMask account and login. Then switch the network to ```Rinkeby Test Network``` and allow our site to access your account by going to MetaMask -> Setting -> Connection. If you need some Ether for testing, you can get it [here](https://faucet.rinkeby.io/).

### 1.3 Run the Project

You can run the project simply by ```npm start```, and the project is running on ```http://localhost:3000/#/```.

All items are showed on the home page. If you want to purchase or sell items, please click the register button to register first. If you want to sell, click the sell button to input the information about your item. If you want to purchase an item, click the details button of the item, then you can view the information and the picture of the item as well as the seller's rating history. After click the purchase button, you can rate the seller.

## 2. Implementation Design

The implementation of our project can be divided into three parts: front-end, smart contract and storage of items’ pictures.

- For the front-end, we used ```web3.js``` as the main library to implement the interaction between front-end and smart contract.
- For smart contract, we used ```solidity``` as the contract language.
- For storage of items’ pictures, we found that storing pictures on blockchain directly is inefficient and may occur error while calling the smart contract function in front-end. Thus, we chose to use ```IPFS``` to store the image file of items and the returned ```IPFS hash``` of image file will be stored on the blockchain.

### 2.1 Smart Contract

We created a smart contract and deployed it to ```Rinkeby``` which is a test network of Ethereum. The contract contains the following functions:

- ```register()``` One seller should register before upload the item on platform. This function will record the address of a seller in our contract.
- ```post_item()``` This function will record the name, description, IPFS hash of item picture, price and seller address of the item in our contract.
- ```purchase()``` This function will record the purchase operation and transfer corresponding token to seller.
- ```rate()``` This function will record the submitted rate in our contract.
- ```getAllItems()``` This function returns all available items’ information.
- ```getRatingHistory()``` This function returns the rating history of a specific seller using the seller address.
- ```getItem()``` This function returns the related information of a specific item according to the inputted ```IPFS hash``` of this item picture.

### 2.2 React Application Framework

We used ```create-react-app``` to build a react application framework. Then we created 3 JavaScript files to setup the configuration: ```web3.js```, ```contract.js```, and ```router.js```.

- ```web3.js``` This is used to setup the ```web3``` configuration.
- ```contract.js``` The frontend can call the contract function through this ```contract.js``` file. We store the ABI of contract and the contract address in this file.
- ```Router.js``` Since ```create-react-app``` only builds a single-page react application, we need to setup a router for navigation between pages. The router configuration is stored in this file.

### 2.3 Request Handling

We created 5 files to handle the requests of the project.

- ```App.js``` In this file, it will call the ```getAllItems()``` stated in smart contract to get all available items in this second hand selling platform. Then, we’ll display all items in this file. This file establishes the home page of our project.
- ```rating.js``` This file is used to handle rating function. In this file, it will call the rate() of contract to submit the buyer’s rate onto blockchain.
- ```ratingHistory.js``` In this file, it will call the ```getRatingHistory()``` of contract with a specific seller address to get the rating history of this seller. Then we’ll display the rating history in this page.
- ```showItem.js``` In this file, it will call the ```getItem()``` of contract with the ```IPFS hash``` of an item picture. Then, it will display all details of this item in the front-end. Moreover, this file handles the purchase function. If customer wants to purchase this item, it will call the ```purchase()``` of contract to submit the purchase transaction onto the blockchain.
- ```uploadItem.js``` This file is used to handle the item upload function. In this file, it will upload the item picture uploaded by seller to ```IPFS``` and get returned ```IPFS hash```. Then it will call the ```postItem()``` of contract to upload the information of an item onto the blockchain. (Information includes ```name```, ```description```, ```IPFS hash``` of the ```item picture```, ```price```, ```seller address```).

