pragma solidity >=0.4.22 <0.6.1;
pragma experimental ABIEncoderV2;


contract secondHandPlatform{
    
    struct Item{
        string picture;
        string name;
        string description;
        uint price;
        address seller;
    } // Item struct

    address[] registered_sellers; // store address of registered sellers
    mapping(address => string[]) rating_history; // store rating history
    mapping(address => address[]) sold_history;
    mapping (string => Item) platform; // store all items info
    Item[] allItems;
    
    
    modifier registered(address user) {
        bool isRegistered = false;
        for (uint i = 0; i < registered_sellers.length; i ++){
            if(registered_sellers[i] == msg.sender){
                isRegistered = true;
            }
        }
        require(isRegistered, "You haven't registered yet");
        _;
    } 
    
    modifier unRegistered(address user) {
        bool isRegistered = false;
        for (uint i = 0; i < registered_sellers.length; i ++){
            if(registered_sellers[i] == msg.sender){
                isRegistered = true;
            }
        }
        require(!isRegistered, "You have registered");
        _;
    }
    
    modifier purchsedItem(address buyer_addr, address seller_addr) {
        bool purchased = false;
        for(uint i = 0; i<sold_history[seller_addr].length; i ++){
           if(sold_history[seller_addr][i] == buyer_addr){
               purchased = true;
           }
        }
        require(purchased, "You cannot rate this seller since you haven't bought anything from this seller.");
        _;
    }
    
    function register() public unRegistered(msg.sender){
        registered_sellers.push(msg.sender);
    }// register new sellers
    
    function post_item(string memory picture, string memory name, string memory description, uint price) public registered(msg.sender){
        Item memory new_item = Item(picture, name, description, price, msg.sender);
        platform[picture] = new_item;
        allItems.push(new_item);
    }// post items
    
    function purchase (uint price, address seller_addr, string memory picture) public payable {
        address payable seller = address (uint160 (seller_addr)); // address => address payable 
        sold_history[seller_addr].push(msg.sender); // add buyer address to the sold list of a seller
        seller.transfer(price);
        uint index = 0;
        for(uint i = 0; i < allItems.length; i ++) {
            if(keccak256(bytes(allItems[i].picture)) == keccak256(bytes(picture))) {
                index = i;
                break;
            }
        }
        for (uint i = index; i < allItems.length-1; i++){
                    allItems[i] = allItems[i+1];
                }
                delete allItems[allItems.length-1];
                allItems.length--;
    }
    
    function rate (string memory rating, address seller_addr) public purchsedItem(msg.sender, seller_addr) {
        rating_history[seller_addr].push(rating);
    }
    
    function getAllItems() public view returns (Item[] memory ) {
        return allItems;
    }
    
    function getRatingHistory(address seller_addr) public view returns (string[] memory) {
        return rating_history[seller_addr];
    }
    
    function getItem(string memory picture_hash) public view returns (Item memory){
        return platform[picture_hash];
    }
}