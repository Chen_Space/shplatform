Function of entire system:
1. Registration. Registered sellers can post selling items.

2. Posting items. Registered sellers can post selling items, each of which is listed with a picture, name, description, price, and the information of the seller.

3. Purchase. A buyer can make a deal on the platform by paying Ether on the website.

4. Rating. After each trading, a buyer can rate the seller, and other buyers can view the rating history of a seller. 

Back-end:
1. Trading data should be put on the blockchain.

2. Store all the data of selling items and the trading and rating history of sellers and buyers